var textdiv = document.getElementById("memes");

// Check of enter is ingedrukt, en zo ja, roep de functie getxp aan
function checkSubmit(e)
{
   if(e && e.keyCode == 13)
   {
      getXp(document.forms[0]);
   }
}

function getXp(form) {
    var xp = form.xp.value;
	if (xp.length != 0){
		if (numbers(xp) === true){
			var xpTo99 = 36073511 - xp;
			var xpTo120 = 80618654 - xp;
			var xpTo200 = 200000000 - xp;
			var rodsTo99 = Math.ceil(xpTo99 / 459000);
			var rodsTo120 = Math.ceil(xpTo120 / 459000);
			var rodsTo200 = Math.ceil(xpTo200 / 459000);
			if (xp < 36073511){
                textdiv.innerHTML = xpTo99 + " xp left to 99 (" + rodsTo99 + " rods)<br>" + xpTo120 + " xp left to 120 (" + rodsTo120 + " rods)<br>" + xpTo200 + " xp left to 200m (" + rodsTo200 + " rods)";
            }
				// alert(xpTo99 + " xp left to 99 (" + rodsTo99 + " rods)\n" + xpTo120 + " xp left to 120 (" + rodsTo120 + " rods)");}
			else if (xp < 80618654) {
                textdiv.innerHTML = xpTo120 + " xp left to 120 (" + rodsTo120 + " rods)<br>"  + xpTo200 + " xp left to 200m (" + rodsTo200 + " rods)";
            }
                // alert(xpTo120 + " xp left to 120 (" + rodsTo120 + " rods)");
			else if (xp < 200000000) {
                textdiv.innerHTML =  xpTo200 + " xp left to 200m (" + rodsTo200 + " rods)";
				// alert("you're already 120, dont boast about it");
            }
			else {
				textdiv.innerHTML = "you're already 200m get outta here"
			}
		}
		else {
            textdiv.innerHTML = "you should really only enter numbers";
		    // alert("you should really only enter numbers");
		}
	}
	else {
		return;
	}
}

function numbers(x) {
	var refString = "0123456789";
	for (i=0; i < x.length; i++)
		{
		testChar = x.substring (i,i+1);
		if (refString.indexOf(testChar, 0)==-1){
			return (false);}
		}
	return (true);
	}
