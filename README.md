# Invention calculator
## voor augmented crystal rods of fishing rod-o-matic

Een rekenmachine om uit te rekenen hoeveel ervaringspunten jij nodig hebt voor niveau 99 of 120 uitvinden.
Vul alleen jouw huidige ervaringspunten in de tekstdoos in en klik op "do ik vis?" en het resultaat verschijnt
midden op het scherm. Het aantal benodigde ervaringspunten en het aantal benodigde vishengels dat je nodig hebt
zijn voor je uitgerekend, zodat je meteen naar de Grote Uitwisseling kan om jouw voorraden te kopen.
